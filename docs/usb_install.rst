.. _usb_install:

USB installer
=============

Although :ref:`our advanced usage documentation <advanced\_usage>` covers
imaging via PXE, you will get your first box up and running quicker with less
fuss using the USB installer.

It's also useful for setting up production boxes and to bootstrap the PXE
server. See :ref:`USB or PXE <usb\_or\_pxe>` for why things are the way they
are.

.. toctree::

   usb_install/usb_quick_start.rst
   usb_install/scripts.rst
   usb_install/usb_or_pxe.rst
   usb_install/mk_usb_installer.rst
