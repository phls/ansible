.. _simple_setup:

Simple setup
============

We do not recommend that you start by trying to build a complete setup using
the 9 different roles documented in the :ref:`general layout <general_layout>`.

Instead, start by building these three machines to get a basic working system:

.. toctree::
   :numbered:

   simple_setup/voctomix.rst
   simple_setup/gateway.rst
   simple_setup/opsis.rst
