# users

Manage user creation, password and SSH access.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `user_name`:             Main user username.

* `user_group`:            Main user group.

* `user_password`:         Main user password in plain text. Please use ansible
                           vault to store this variable.

* `ssh_public_keys`:       List of user key dicts, as described below.

* `ssh_public_keys.[]`:    A dict representing a person or group of keys.

* `ssh_public_keys.[].keys`: List of authorized SSH keys.

* `ssh_public_keys.[].groups`: Optional List of groups that these keys
                               should be restricted to. If not specified,
                               the keys will be deployed everywhere.

* `ssh_public_keys.[].root`: Optional Boolean. If False, these keys will
                             only give access to the main user, not root.

* `ssh_private_key`:       ed25519 SSH private key to install for the main user.

* `time_zone`:             Timezone using the tzdata format.

* `ssh_password_auth`:     Boolean. If true, users can login via SSH with a
                           password.
