# motd

This modules configures and manages a personalised Message of the Day.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `conference_name`:    Name of your conference or event.

* `conference_town`:    Name of the town where your conference or event takes
                        place.

* `conference_country`: Name of the country where your conference or event takes
                        place.
