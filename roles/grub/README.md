# grub

Module to configure Linux boot options.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

List of the role's variables. Please follow the following style:

Main variables are:

* `grub_linux_cmdline`: String. The line that should be used for
                        GRUB_CMDLINE_LINUX in grub.
