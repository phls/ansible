# staticips

Manages `/etc/interface` and `/etc/hosts` files.
Using this module disables the hosts management function in the
`pxe/dhcp-server` role.

## Tasks

Tasks are separated in two different parts:

* `tasks/interfaces.yml` manages the network interface.

* `tasks/hosts.yml` manages the hosts file.

## Available variables

Main variables are:

* `staticips.gateway`:        Gateway for the DHCP server to advertise
                              (Default: self)
* `staticips.hosts`:          A list of machines that may be PXE imaged.

  Each item is a dictionary of:
  - `hostname`:               Hostname of the machines.
  - `ip`:                     IP Address.
  - `fqdn`:                   Fully-qualified domain name. If not present,
                              defaults to `hostname` concatenated with the
                              domain name configured for the setup.
  - `aliases`:                Any aliases to be configured for the host in
                              hosts files.
  - `mac`:                    Ethernet MAC address.
  - `comment`:                A text comment field.
  - `tasks`:                  List of extra tasks to install.
  - `disk`:                   Path to the primary install disk.
                              (Defaults to the `default_install_disk`
                               variable)
  - `noipxe`:                 Disable chain load the iPXE client.
                              DebConf videos' HP laptops don't seem to
                              like it.
  - `noshim`:                 Disable chain loading the shim.
                              DebConf23 MSI motherboards don't seem to
                              like it.
  - `pxeflags`:               List of flags for pxe boot.
    The flags can be:
    - `force-autoinstall`:    Force automatic installation on pxe boot.
                              When the mac address is set, this will cause d-i
                              to start and wipe the machine. USE WITH CAUTION!
                              When the mac address is not set, d-i will start
                              and ask for the hostname.
  - `interfaces_extra_config`: List of extra lines to add to the
                               `/etc/network/interfaces` entry.
* `staticips.write_hosts`:    Boolean. Write the contents of
                              `staticips.hosts` into `/etc/hosts`.
* `staticips.write_interfaces: Boolean. Write the machine's details into
                              `/etc/network/interfaces` rather than using
                              DHCP.
