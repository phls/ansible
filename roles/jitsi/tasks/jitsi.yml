---
- name: Install gnupg
  ansible.builtin.apt:
    name: gnupg

- name: Add Jitsi apt key
  ansible.builtin.copy:
    src: jitsi-repo.gpg
    dest: /usr/share/keyrings/jitsi-repo.gpg
  tags:
    - jitsi

- name: Add Jitsi apt repo
  ansible.builtin.apt_repository:
    repo: >
      deb [signed-by=/usr/share/keyrings/jitsi-repo.gpg]
      https://download.jitsi.org stable/
  tags:
    - jitsi

- name: Pin Jitsi packages
  ansible.builtin.copy:
    src: jitsi-apt-pin
    dest: /etc/apt/preferences.d/jitsi-apt-pin
  tags:
    - jitsi

- name: Configure debconf options for Jitsi
  ansible.builtin.debconf:
    name: "{{ item.name }}"
    question: "{{ item.question }}"
    value: "{{ item.value }}"
    vtype: "{{ item.vtype }}"
  with_items:
    - name: jitsi-meet
      question: jitsi-meet/jvb-serve
      value: "false"
      vtype: boolean
    - name: jitsi-meet-prosody
      question: jitsi-meet-prosody/jvb-hostname
      value: "{{ jitsi_meet_domain }}"
      vtype: string
    - name: jitsi-videobridge
      question: jitsi-videobridge/jvb-hostname
      value: "{{ jitsi_meet_domain }}"
      vtype: string
    - name: jitsi-meet-web-config
      question: jitsi-meet/cert-choice
      # It is recommended to keep this, even though we generate certs
      value: Generate a new self-signed certificate (You will later get a chance
             to obtain a Let's encrypt certificate)
      vtype: select
    - name: jitsi-meet-web-config
      question: jitsi-meet/cert-path-key
      value: /etc/ssl/ansible/private/jitsi.key
      vtype: string
    - name: jitsi-meet-web-config
      question: jitsi-meet/cert-path-crt
      value: /etc/ssl/ansible/certs/jitsi.fullchain.pem
      vtype: string
  tags:
    - jitsi

- name: Install Jitsi and dependencies
  ansible.builtin.apt:
    name: jitsi-meet
  tags:
    - jitsi

- name: Read jicofo configuration
  ansible.builtin.slurp:
    src: /etc/jitsi/jicofo/config
  register: jicofo_config
  tags:
    - jitsi

- name: Read videobridge sip communicator
  ansible.builtin.slurp:
    src: /etc/jitsi/videobridge/sip-communicator.properties
  register: videobridge_config
  tags:
    - jitsi

- name: Read prosody config
  ansible.builtin.slurp:
    src: /etc/prosody/conf.d/{{ jitsi_meet_domain }}.cfg.lua
  register: prosody_config
  tags:
    - jitsi

- name: Set Jitsi internal password variables
  ansible.builtin.set_fact:
    jicofo_password: "{{ jicofo_config['content'] | \
                      b64decode | \
                      regex_search('JICOFO_AUTH_PASSWORD=(.*)') | \
                      regex_replace('.*=(.*)', '\\1') }}"
    videobridge_password: "{{ videobridge_config['content'] | \
                           b64decode | regex_search('shard.PASSWORD=(.*)') | \
                           regex_replace('.*=(.*)', '\\1') }}"
    videobridge_uuid: "{{ videobridge_config['content'] | \
                       b64decode | regex_search('shard.MUC_NICKNAME=(.*)') | \
                       regex_replace('.*=(.*)', '\\1') }}"
    turn_secret: "{{ prosody_config['content'] | \
                  b64decode | \
                  regex_search('turncredentials_secret = (.*);') | \
                  regex_replace('.*= \"(.*)\";', '\\1') }}"
  tags:
    - jitsi

- name: Create prosody state directory
  ansible.builtin.file:
    dest: /etc/prosody/ansible-account-states
    mode: "0700"
    state: directory
  tags:
    - jitsi

- name: Update jvb user
  ansible.builtin.copy:
    content: "{{ videobridge_password | string | hash('sha3_256') }}\n"
    dest: /etc/prosody/ansible-account-states/jvb
  notify: Register jvb user
  tags:
    - jitsi

- name: Update jicofo user
  ansible.builtin.copy:
    content: "{{ jicofo_password | string | hash('sha3_256') }}\n"
    dest: /etc/prosody/ansible-account-states/focus
  notify: Register jicofo user
  tags:
    - jitsi

- name: Read jigasi configuration
  ansible.builtin.slurp:
    src: /etc/jitsi/jigasi/config
  register: jigasi_config
  ignore_errors: true
  tags:
    - jitsi

- name: Set Jitsi internal password variables (jigasi)
  ansible.builtin.set_fact:
    jigasi_secret: "{{ jigasi_config['content'] | \
                    b64decode | \
                    regex_search('JIGASI_SECRET=(.*)') | \
                    regex_replace('.*=(.*)', '\\1') }}"
  when: jigasi_config.content is defined
  tags:
    - jitsi

- name: Setup prosody configuration
  ansible.builtin.template:
    src: prosody.cfg.lua.j2
    dest: /etc/prosody/conf.avail/{{ jitsi_meet_domain }}.cfg.lua
  notify: Reload jitsi components
  tags:
    - jitsi

- name: Enable prosody configuration
  ansible.builtin.file:
    src: /etc/prosody/conf.avail/{{ jitsi_meet_domain }}.cfg.lua
    dest: /etc/prosody/conf.d/{{ jitsi_meet_domain }}.cfg.lua
    state: link
  notify: Reload jitsi components
  tags:
    - jitsi

- name: Configure Jitsi videobridge
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: /etc/jitsi/videobridge/{{ item.dest }}
    owner: jvb
    group: jitsi
  with_items:
    - src: videobridge-communicator.properties.j2
      dest: sip-communicator.properties
    - src: videobridge-config.j2
      dest: config
  notify: Reload jitsi components
  tags:
    - jitsi

- name: Configure Jitsi jicofo
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: /etc/jitsi/jicofo/{{ item.dest }}
    owner: jicofo
    group: jitsi
  with_items:
    - src: jicofo-communicator.properties.j2
      dest: sip-communicator.properties
    - src: jicofo-config.j2
      dest: config
  notify: Reload jitsi components
  tags:
    - jitsi

- name: Configure Jitsi meet
  ansible.builtin.template:
    src: meet-config.js.j2
    dest: /etc/jitsi/meet/{{ jitsi_meet_domain }}-config.js
    mode: "0644"
  tags:
    - jitsi
