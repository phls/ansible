---
- name: Install the tftp server
  ansible.builtin.apt:
    name: dnsmasq

- name: Enable tftp
  ansible.builtin.copy:
    src: files/tftp.conf
    dest: /etc/dnsmasq.d
  notify: Restart dnsmasq

- name: Configure dnsmasq for PXE
  ansible.builtin.copy:
    src: files/pxe.conf
    dest: /etc/dnsmasq.d/
  notify: Restart dnsmasq

- name: Create boot image directories
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    mode: "0755"
  with_items:
    - /srv/tftp
    - /srv/tftp/ipxe

- name: Install grub i386-pc, even if we are a UEFI install
  ansible.builtin.apt:
    name: grub-pc-bin

- name: Magically build /srv/tftp/boot/grub
  ansible.builtin.command: grub-mknetdir
  args:
    creates: /srv/tftp/boot/grub/i386-pc/core.0

- name: Write grub.cfg
  ansible.builtin.template:
    src: grub.cfg.j2
    dest: /srv/tftp/boot/grub/grub.cfg

- name: Create debian suite dirs
  ansible.builtin.file:
    path: /srv/tftp/debian/{{ item[0] }}/{{ item[1] }}
    state: directory
    recurse: true
    mode: "0755"
  with_nested:
    - "{{ debian_suites }}"
    - "{{ archs }}"

- name: Create ubuntu suite dirs
  ansible.builtin.file:
    path: /srv/tftp/ubuntu/{{ item[0] }}/{{ item[1] }}
    state: directory
    recurse: true
    mode: "0755"
  with_nested:
    - "{{ ubuntu_suites }}"
    - "{{ archs }}"

- name: Download debian netboot images
  ansible.builtin.get_url:
    url: "http://{{ debian_host }}/debian/dists/{{ item[0] }}/main/\
      installer-{{ item[1] }}/current/images/netboot/debian-installer/\
      {{ item[1] }}/{{ item[2] }}"
    dest: /srv/tftp/debian/{{ item[0] }}/{{ item[1] }}/{{ item[2] }}
    mode: "0644"
  with_nested:
    - "{{ debian_suites }}"
    - "{{ archs }}"
    - [linux, initrd.gz]

- name: Download ubuntu netboot images
  ansible.builtin.get_url:
    url: "http://{{ ubuntu_host }}/ubuntu/dists/{{ item[0] }}/main/\
      installer-{{ item[1] }}/current/images/netboot/ubuntu-installer/\
      {{ item[1] }}/{{ item[2] }}"
    dest: /srv/tftp/ubuntu/{{ item[0] }}/{{ item[1] }}/{{ item[2] }}
    mode: "0644"
  with_nested:
    - "{{ ubuntu_suites }}"
    - "{{ archs }}"
    - [linux, initrd.gz]

# When d-i updates are available in the -updates pocket, we should use them,
# because the images from the release pocket will not work
- name: Download ubuntu updated netboot images
  ansible.builtin.get_url:
    url: "http://{{ ubuntu_host }}/ubuntu/dists/{{ item[0] }}-updates/main/\
      installer-{{ item[1] }}/current/images/netboot/ubuntu-installer/\
      {{ item[1] }}{{ item[2] }}"
    dest: /srv/tftp/ubuntu/{{ item[0] }}/{{ item[1] }}/{{ item[2] }}
    mode: "0644"
  ignore_errors: true  # noqa: ignore-errors
  with_nested:
    - "{{ ubuntu_suites }}"
    - "{{ archs }}"
    - [linux, initrd.gz]

- name: Install secure PXE Boot dependencies
  ansible.builtin.apt:
    name:
      - grub-efi-amd64-signed
      - shim-signed

- name: Make the signed UEFI grub available
  ansible.builtin.copy:
    remote_src: true
    src: /usr/lib/grub/x86_64-efi-signed/grubnetx64.efi.signed
    dest: /srv/tftp/grubx64.efi
    mode: "0644"

- name: Make the signed UEFI shim available
  ansible.builtin.copy:
    remote_src: true
    src: /usr/lib/shim/shimx64.efi.signed
    dest: /srv/tftp/boot/bootx64.efi
    mode: "0644"

# Haven't figured out how to give UEFI grub a prefix, yet.
- name: /grub symlink for UEFI PXE
  ansible.builtin.file:
    state: link
    src: /srv/tftp/boot/grub
    dest: /srv/tftp/grub

- name: Download ipxe boot loaders
  ansible.builtin.get_url:
    url: https://boot.ipxe.org/{{ item }}
    dest: /srv/tftp/ipxe/{{ item }}
    mode: "0644"
  with_items:
    - undionly.kpxe
    - ipxe.efi
    - ipxe.lkrn
    - ipxe.pxe
#   - ipxe.efi.signed  # nope. see http://forum.ipxe.org/showthread.php?tid=7533
