# opsis-ingest

Depends on the `opsis` role.

Feed video from an Opsis into a voctomix / nageru.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `mixer`:           "voctomix" or "nageru"

* `ingest.host`:     Hostname or IP of the mixer machine.

* `ingest.port`:     Incoming port the mixer machine listens on.

* `video_delay`:     Delay in ms for the video capture.

* `alsa_device`:     ALSA device used for USB audio capture.

* `audio_delay`:     Delay in ms for the audio capture.

* `video_source`:    ingest `--video-source`. Default: `hdmi2usb`.

* `monitor`:         GST video sink element to display stream.  Default: none.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
